# Lab vunt_java_gradle_helloworld

This is a simple app using Gradle.
## Getting Started

This guide walks you through using Gradle to build a simple Java project.


### Set up the envirement

* [Spring Tools 4 for Eclipse](https://spring.io/tools)
* [Gradle](https://gradle.org/install/)

### Installing

Open project by Spring Tools 4 ()

```
File -> Import -> Gradle -> Existing Gradle Project
```

## Running the tests

```
Run As -> Java Application -> Main - helloworld
```

### Build with Gradle


```
Gradle Task -> build -> build
```

### See Also
*[Gradle document](https://spring.io/guides/gs/gradle/#initial)